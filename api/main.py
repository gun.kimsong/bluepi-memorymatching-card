from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from datetime import datetime
from pydantic import BaseModel
import pyrebase
import moment

config = {
  "apiKey": "AIzaSyD41inxDquUGEt9OYpVB5EW9JTW0bs45yE",
  "authDomain": "bluepi-memorygame.firebaseapp.com",
  "databaseURL": "https://bluepi-memorygame.firebaseio.com",
  "storageBucket": "bluepi-memorygame",
  "serviceAccount": "../serviceAccountCredentials.json"
}

firebase = pyrebase.initialize_app(config)

app = FastAPI()
origins = [
    "http://localhost",
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_methods=["*"],
    allow_headers=["*"],
)

db = firebase.database()

class UpdateScore(BaseModel):
    display_name: str
    profile_url: str
    user_id: str
    score: int

class Profile(BaseModel):
    name: str
    lastname: str
    email: str

@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.get("/items/")
def read_item():
    data = {"name": "Mortimer 'Morty' Smith"}
    users = db.child("users").get()
    return users.val()

@app.post("/scores")
def update_score(score: UpdateScore):
    try:
        if score.score>12 :
            update_myrank(score)
            update_global(score)
            return True
        else:
            print("Are you cheated ?")
            return False
    except:
        print("An exception error")
        return False

def update_myrank(score: UpdateScore):
    try:
        now = moment.now().format("YYYYMDhhmmss")
        apiPath = "history/" + score.user_id + "/" + str(now) + "/"
        data = {
                "score":score.score
                }
        db.child(apiPath).update(data)
        return True
    except:
        return False

def update_global(score: UpdateScore):
    try:
        now = moment.now().format("YYYYMDhhmmss")
        print('update global')
        apiPath = "ranking/" + str(score.score) + "/" + score.user_id + "/"
        data = {
                "display_name":score.display_name,
                "profile_url":score.profile_url,
                "time":str(now)
                }
        db.child(apiPath).update(data)
        return True
    except:
        return False
        