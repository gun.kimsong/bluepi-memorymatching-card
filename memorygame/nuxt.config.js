
require("dotenv").config({
  path: process.env.NODE_ENV !== "production" ? "local.env" : ".env"
});
module.exports = {
  mode: 'universal',
  server: {
    port: process.env.PORT || 3000,
    host: '0.0.0.0',
  },
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: ["@/assets/main.scss"],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: "~/plugins/vue-sweetalert.js", ssr: false },
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/moment'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    [
      "@nuxtjs/dotenv",
      { filename: process.env.NODE_ENV !== "production" ? "local.env" : ".env" }
    ],
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
        '@nuxtjs/axios',
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  },
  axios: {
    // proxyHeaders: false
  }
}
