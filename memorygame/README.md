# MemoryGame

> bluePi memorygame exam

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

# Deploy to cloud run

# Buid container to google cloud docker registry
- gcloud builds submit --project "bluepi-memorygame" --config=./cloud-build.yml

# Deploy
- gcloud run deploy exam --region asia-northeast1 --project "bluepi-memorygame" --platform=managed --image gcr.io/bluepi-memorygame/exam --allow-unauthenticated;

# Retrafiic to new container 
gcloud alpha run services update-traffic exam --to-latest;