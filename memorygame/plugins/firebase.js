import firebase from 'firebase/app'
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';
import 'firebase/database'




var config = {
    apiKey: process.env.APIKEY,
    authDomain: process.env.AUTHDOMAIN,
    databaseURL: process.env.DATABASEURL,
    projectId: process.env.PROJECTID,
    storageBucket: process.env.STORAGEBUCKET,
    messagingSenderId: process.env.MESSAGINGSENDERID,
    appId:process.env.APPID
}

export default !firebase.apps.length ? firebase.initializeApp(config) : firebase
export const fireAuth = firebase.auth()
export const fireDb = firebase.firestore()
export const fireStorage = firebase.storage()
export const fireRDB = firebase.database();
export const fireDbFunc = firebase.firestore

export const GoogleAuthProvider = new firebase.auth.GoogleAuthProvider()
export const FacebookAuthProvider = new firebase.auth.FacebookAuthProvider()
