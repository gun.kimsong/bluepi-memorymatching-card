import {fireAuth} from './firebase'

import {USER_REQUEST} from '@/store/actions/user'
import {CALL_NOTIFICATION} from '@/store/actions/notification'

export default ({app} ) => {

    
    
    return fireAuth.onAuthStateChanged((user) => {
        // console.log(user)
        if (user !== null) {
            // app.store.commit('AUTH_SUCCESS')
            app.store.dispatch(USER_REQUEST, user)
            app.store.dispatch(CALL_NOTIFICATION, app.store.getters.defaultNotifications.loggedInMessage)
        } 
        // else {
        // //     // app.store.commit('AUTH_ERROR')
        //     // app.store.dispatch(USER_REQUEST, null)
        // //     app.store.dispatch(CALL_NOTIFICATION, {
        // //         message: 'Hi Anonymous!',
        // //         color:'default'})
        // }
    });
}
