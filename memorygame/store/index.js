import * as auth from './modules/auth'
import * as user from './modules/user'
import * as notification from './modules/notification'

import {fireDb} from '@/plugins/firebase';

import {UI_DISABLED,SET_IP,SET_APP_DATA} from './actions/interface'

export const state = () => ({
  UIDisabled: false,
  ip: false,
  data: null
})

export const getters ={
  getUIStatus: state => state.UIDisabled,
  getIPClient : state => state.ip,
  getData: state=> state.data

}
export const mutations = {
  [UI_DISABLED] (state,payload) {
    state.UIDisabled = payload
  },
  [SET_IP] (state,payload) {
    state.ip = payload
  },
  [SET_APP_DATA] (state,payload) {
    state.data = payload
  }

}
export const modules ={
  auth,
  user,
  notification,
}

export const actions = {
  async nuxtServerInit ({ commit,dispatch }, { req }) {
    let user_ip;

    // 'x-forwarded-for'
    if(req.headers['x-real-ip'] && req.headers['x-real-ip'].split(', ').length) {
      let first = req.headers['x-real-ip'].split(', ');
      user_ip = first[0];
    } else {
      user_ip = req.headers['fastly-client-ip'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress ||req.ip;
    }
      commit(SET_IP, user_ip )

      // let url = '/api'
      // if(process.env.NODE_ENV === 'production'){
      //   url =  req.headers['x-forwarded-proto']+"://" + req.headers['host']+ '/nuxtvue' + url
      // }      
      // const { data } = await this.$axios.get(url)
      const d =await dispatch('initData')
      commit(SET_APP_DATA,d)
  },
  async initData(){

  }
}