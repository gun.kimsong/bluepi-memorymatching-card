export const CALL_NOTIFICATION = 'CALL_NOTIFICATION'
export const SET_NOTIFICATION = 'SET_NOTIFICATION'
export const START_NOTIFICATION = 'START_NOTIFICATION'
export const CLOSE_NOTIFICATION = 'CLOSE_NOTIFICATION'