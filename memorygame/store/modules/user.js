import {
    USER_REQUEST,
    USER_ERROR,
    USER_SUCCESS,
    SET_USER,
    INIT_USER,
    INIT_INFO_USER
  } from '@/store/actions/user'
  import {fireDb} from '@/plugins/firebase';

  const imgDefault = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAYFBMVEX///+AgIB9fX16enp3d3fz8/OGhoaNjY3u7u76+vqBgYHf39+Xl5f8/Pz29vaEhITGxsbMzMzo6OigoKCoqKjW1taampq6urqurq6QkJDKysq3t7fc3NyioqK+vr7k5OTwp0xfAAAGbElEQVR4nO2d2ZaqMBBFmyRMCg4IiOLw/395odV2aFoTkhq4K/vJR8+qkBpSqXx9eTwej8fj8Xg8Ho/H4/F4PB6Px+PxeDwej8fj8Xg8Ho/HhDRMzuvtMYuFVErJWVyXp3WRhNT/yxHhYd9kkZJSCBFc6H5JKeO6Xc9T6r9nTdXsoru0Z4QUUd3Oqf+iBeGhVOoPdXeVataep2nJZJ99UHdDinK9oP67xuTtTlPftyVnuzX1PzYj2XbfmLa+b40q2k9nrabrWBrJu6COFfU/16TYBGb2+7Hjssyp/7wGi5Map69HztbsNZ6P4/X1ZhQN80jnEFkJ7M24KahFvCFtDXfQYTPydRxpaa+vR+6ZfoyLcoyPGORErWWQInMmMFAc3UboUGAnsWUX4CyOLgV2+82WWtEL4catwN6K1JqeaVwL7CSychp79wKDIGKU/VczAIGBiNlEN8nOjaf/JbGmVnZjAyOw+xRX1NIurBSQwCBYHqjF9RQxlAm7dbphUKHKGziBnUQG67SCcBR3FL0RAddoj2yoBa7htpkL0ZlWYAjkCu/IhjaR2gPr65iRGnEB5uzv0H6JFfRX2EO6ndbwJuyMSOgTFxgm7AJwuorGFtbb31iSndjkKIu0W6ZkNZt5hCKwW6ZUZxkrnEXaGZHIJboq4msoJKqBJ8BB9x2R0Sico/iKbxSNwhPWZ9gppPkQ7U57jZB7EoUgRdJhBEn0neN9hkSV0wJTYUbh81EypxskBX60iKYnoigNIzoLovQCtBL8C4rDRLSotEdQOMT/XyFiSENUq0EoJBIrxLWh/w4h+P+9RYvp8WcUHv//j9qAD3+fiSmS/DNm9rRLCBSmqBkwyTEp4l4qSgqBXxliJYqmJIx08tSjaHqjDogVYZoTxGSJpjAiEfi1QIu9JVVHNFqlRlK1KFZIy1TEFP6+Z4HkL0RL1haFlF6QJBYXcEJTmqD0CnjfXg/ZTtoDcs/iFUVoQpSjfKpD/CsrBIW0V2cgG/WvAo/End7gDoPQVVzIgQUS5b6PAJfcFP3trhA0dJP0JuyHDMAJFDsWExYAN5sZj3uk+QxqnUoul9bPS6AblkTFiwFWIAJpDiuGyd3Ni3hAsJodAeAy6PrXB3F/2VluqDW9MLcevfMisKbMCgcpnG6ocsPC1T9zcJhISZJuy4+4W6gMl+gFVxdKZcNuOM2NxMlJhmA2t+WJtLUYR3fVF3Cdg3WlstxvSMu/ehSlRSFcBC3LTfSZdBWMjFKFXM55r9AbYTPK+8uYVaj9nqo0GEB7NWDU0BedTDjvDAVmPAoWeiySdWlc2RBS1atiAvvMVzo/bZbjvKKQom4r+nEtbwm3dWQz4VPIWcZ4hnm4zpT9ANNuuUYrlm6/OGXWIduPyIifIcM2NnYQb0VGR07OI0+Oynm1TajswCXCKbYRSGFfyIbFWs1XcOfAIirJk+G8ilztL4NIsaKNAorGwXzrDxpryqUKuEDviIisr62oQRfoHRnQmHGPNvkjELMTfriat049/EeNNbbE8w7zUlAvEbmxpnJ8EKMlEfO4bQV0rv0evFp4alMwtJKY4UTjCciZthY4A7BDpCltwxJ38J4xAeue0ZMooLfUM0pr91uNsBKhxq+bKIwgJSaoccxfEgGf2ikQL1W+Q0A1S5Huoo+IGGZHTfFyiU+IJYREh48d2QNyna1lY8Ee6f5NgRNRLPoXsnQsEfFasyaOL3zB340xR7r0/AkTR/iEcPmYEKdt9I7DCcqog1oMcDb0E2uqtTmO3vXCeABhJI5y/i1bgf3VNgfFKcxZQuYo+z6qBZeE4g+EdYCKcNPXCusrmHNqBR+xffSKYzDzjGUihToOeSRWT+3gPQ5gg7KITzm7wjtiM9opJmzDtRdG51F8w7VnRjf4845mHhk7hBd1yqwlowTCvIELw6gLp3gP5Thg1Jd4npDAcbEb/3jtkRGxG+YDHS4w305xh1k7wHQAw3kq4cwPplWp09RM2EWnRj2asHN1YDAbowH+zC8A0qjRlr7lYgQmDiOZoAk7IxrsNQ3Tg4r3iFhb4BT3mR79GZJY43Ndo51h5NNcpAbDz6ZRYRtC92kvrgein9Gdvz+VAtRvdF8RnKwJO4lab5kgvhHrHL3ddHKZ4SM6WSLWpHUYlMaHyLfzQged2JT7oe97xPGjwHTSn6FOWMOgG9+K35n+P/x2e/1zl+p7AAAAAElFTkSuQmCC'
  export const state = () => ({
    uid: null,
    displayName: null,
    name:null,
    email: null,
    telNo: null,
    photoURL: imgDefault,
    loggedIn: false,
    emailVerified: false,
    providerData: [],
    isAnonymous:true,
    isAdmin:null,
  })
  export const getters = {
    // getProfile: state => state.profile,
    isLoggedIn: state => state.loggedIn,
    stateUser: state => {
        return { uid:state.uid , 
          displayName : state.displayName ,
          name:state.name,
          email : state.email ,
          telNo: state.telNo,
          photoURL:state.photoURL , 
          isAdmin : state.isAdmin
        }
    }
  }
  
  export const actions = {
    [INIT_USER]: ({
      commit,
      dispatch
    },user) => {
      return new Promise( async (resolve, reject) => {
        const dbUser = fireDb.collection('users')
        let checkDB = false
          const data = await dbUser.doc(user.uid).get().then(function(doc) {
            if (doc.exists) {
              // console.log("Document data:", doc.data());
              return doc.data()
            } else {
                // doc.data() will be undefined in this case
                console.log("No such document!");
                checkDB =true
                return false;
            }
        }).catch(function(error) {
            // console.log("Error getting document:", error);
        });
        if(data){
            commit(INIT_INFO_USER, data)
        }else{
          if(checkDB){
            await dbUser.doc(user.uid).set({isAdmin:false,email:user.email,displayName: user.displayName || user.email,telNo:'',name:''}) // init new user
          }
        }
          resolve(true)
      });
    },
    [USER_REQUEST]: ({
      commit,
      dispatch
    }, user) => {
      commit(USER_REQUEST)
      if (user !== null) {
      // apiCall({url: 'user/me'})
      //   .then(resp => {
      dispatch(INIT_USER,user)
      commit(SET_USER, user)
      }else{
      commit(SET_USER, null)
        
      }
      commit(USER_SUCCESS)
      //   })
      //   .catch(resp => {
      //     commit(USER_ERROR)
      //     // if resp is unauthorized, logout, to
      //     dispatch(AUTH_LOGOUT)
      //   })
    }
  }
  
  export const mutations = {
    [USER_REQUEST]: (state) => {
      state.statusSignin = 'loading'
    },
    [INIT_INFO_USER]: (state,data) => {
      state.isAdmin = data.isAdmin
      if(data.telNo){
        state.telNo = data.telNo
      }
      if(data.name){
        state.name = data.name
      }else{
        state.name = state.displayName
      }
    },
    [SET_USER] (state, user) {
      if (user) { // Send through the bits of the user that we need.
        state.uid = user.uid
        state.displayName = user.displayName || user.email
        state.email = user.email
        // need check db before set image show
        state.photoURL = user.photoURL
        state.loggedIn = true
        state.emailVerified = user.emailVerified
        state.providerData= user.providerData
  
        state.isAnonymous = user.isAnonymous
      } else {
        state.uid = null
        state.displayName = null
        state.email = null
        state.photoURL = imgDefault
        state.loggedIn = false
        state.emailVerified = false
        state.providerData= []
        
        state.isAnonymous =true
        state.isAdmin =null
        state.telNo = null
        state.name = null

      }
    },
    [USER_SUCCESS]: (state, resp) => {
      state.statusSignin = 'success'
    },
    [USER_ERROR]: (state) => {
      state.statusSignin = 'error'
    }
  }