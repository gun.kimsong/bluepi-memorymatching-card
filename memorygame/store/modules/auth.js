import {
    AUTH_REQUEST,
    AUTH_ERROR,
    AUTH_SUCCESS,
    AUTH_LOGOUT,
    USER_PROVIDER_SIGNIN
  } from '@/store/actions/auth'
  import {
    USER_REQUEST,
    SET_USER,

  } from '@/store/actions/user'
  import {
    CALL_NOTIFICATION
  } from '@/store/actions/notification'
  import {UI_DISABLED} from '@/store/actions/interface'
  import {fireAuth,GoogleAuthProvider,FacebookAuthProvider} from '@/plugins/firebase';

  export const state = () => ({
  
  })
  export const getters = {
  
  }
  export const actions = {
    [AUTH_REQUEST]: ({commit,dispatch}, credentials) => {
      return new Promise((resolve, reject) => {
        commit(AUTH_REQUEST)
        commit(UI_DISABLED, true)
        const email = credentials.email
        const pass = credentials.pass
        // If there's no current user
        if (!fireAuth.currentUser) {
          fireAuth.signInWithEmailAndPassword(email, pass).then(function (result) {
            commit(UI_DISABLED, false)
            dispatch(USER_REQUEST, result.user)
            commit(AUTH_SUCCESS)
            dispatch(CALL_NOTIFICATION, getters.defaultNotifications.loggedInMessage)
            resolve(result.user)
          }).catch(function (error) {
            commit(UI_DISABLED, false)
            const errCode = error.code
            // Handle Errors here.
            // commit(types.SET_ERROR, error)
            // [START_EXCLUDE]
            if (errCode === 'auth/account-exists-with-different-credential') {
              dispatch(CALL_NOTIFICATION, getters.defaultNotifications.accountExistsDifferentWithCredential)
              // If you are using multiple auth providers on your app you should handle linking
              // the user's accounts here.
            }else if (errCode ==='auth/invalid-email'){ // new user
              dispatch(CALL_NOTIFICATION,{
                color: 'error',
                message: error.message,
                duration: 5000,
                disabledBtn:false
              })
            }else if (errCode == 'auth/user-not-found'){
              dispatch(CALL_NOTIFICATION,{
                color: 'error',
                message: error.message,
                duration: 5000,
                disabledBtn:false
              })
            }
            //auth/user-not-found  !! not found user
            else if (errCode === 'auth/wrong-password') {
              // console.error(error)
              dispatch(CALL_NOTIFICATION,{
                color: 'error',
                message: error.message,
                duration: 5000,
                disabledBtn:false
              })
            } else {
              // console.log(error)
            }
            reject(error)
            // [END_EXCLUDE]
          })
          // [END signin]
        }
      })
    },
      [USER_PROVIDER_SIGNIN]: ({getters, commit, dispatch, state},signInMethod)=> {
      return new Promise((resolve, reject) => {
  
  
        commit(AUTH_REQUEST)
        commit(UI_DISABLED, true)
        // If there's no current user
        if (!fireAuth.currentUser) {
          // [START createprovider]
          //       if (!firebase.auth().currentUser) {
          let provider = {}
          if (signInMethod === 'google') {
            provider = GoogleAuthProvider
          } else if (signInMethod === 'facebook') {
            provider = FacebookAuthProvider
          }  else {
            provider = GoogleAuthProvider
          }
  
          // [END createprovider]
          // [START signin]
          fireAuth.signInWithPopup(provider).then(function (result) {
            // Disable login button
            // commit(types.SET_UI_LOGIN_DISABLED, true)
            // Set logged in within auth state
            // commit(types.SET_LOGGED_IN, true)
            // Add the signed-in user info to state
            dispatch(USER_REQUEST, result.user)
            commit(AUTH_SUCCESS)
            // Finish authing
            // commit(types.SET_AUTH_PENDING, false)
            // commit(types.SET_UI_LOGIN_BTN_TEXT, 'Logged in with Google.')
            commit(UI_DISABLED, false)
            dispatch(CALL_NOTIFICATION, getters.defaultNotifications.loggedInMessage)
            resolve(result)
            // Dispatch the default notification for logging in
          }).catch(function (error) {
            // Handle Errors here.
            // commit(types.SET_ERROR, error)
            // [START_EXCLUDE]
            const errCode = error.code
            if (errCode === 'auth/account-exists-with-different-credential') {
              dispatch(CALL_NOTIFICATION,getters.defaultNotifications.accountExistsDifferentWithCredential)
              // If you are using multiple auth providers on your app you should handle linking
              // the user's accounts here.
              // console.error(error)
            }else if (errCode === 'auth/popup-closed-by-user'){
              dispatch(CALL_NOTIFICATION,{
                color: 'error',
                message: error.message,
                duration: 5000,
                disabledBtn:false
              })
            }
            commit(UI_DISABLED, false)
            reject(error)
            // [END_EXCLUDE]
          })
          // [END signin]
        }
      })
      },
    [AUTH_LOGOUT] ({getters, commit, dispatch, state}) {
      return new Promise((resolve, reject) => {
      // Update firebase ref users/uid/loggedIn boolean to false
      // const userRef = firebase.database().ref('users/' + getters.user.uid)
      // userRef.update({'loggedIn': 'false'})
      // // Set the auth.user state to null
      commit(UI_DISABLED, true)
      setTimeout(()=>{
        dispatch(CALL_NOTIFICATION, getters.defaultNotifications.loggedOutMessage)
        commit(SET_USER, null)
        fireAuth.signOut()
        commit(UI_DISABLED, false)
        resolve(true)
      },2000)
      // // Set logged in within auth state
      // commit(types.SET_LOGGED_IN, false)
      // commit(types.SET_UI_LOGIN_BTN_TEXT, 'Sign in with Google.')
      // Signout of Firebase
      // resolve(this.$route)
      // router.push('/') // Redirect to home after logout
  
      // Dispatch the default notification for logging out
      })
  
    }
  
  }
  
  export const mutations = {
    [AUTH_REQUEST]: (state) => {
      state.status = 'loading'
    },
    [AUTH_SUCCESS]: (state) => {
      state.status = 'success'
    },
    [AUTH_ERROR]: (state) => {
      state.status = 'error'
    }
  
  }