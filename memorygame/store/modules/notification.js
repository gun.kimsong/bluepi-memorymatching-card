import {
    CALL_NOTIFICATION,
    SET_NOTIFICATION,
    START_NOTIFICATION,
    CLOSE_NOTIFICATION
} from '@/store/actions/notification'

export const state = () => ({
    message: '',
    snackbar: false,
    color: 'primary',
    duration: 6000,
    disabledBtn: true,
    defaultNotifications: {
        loggedInMessage: {
            id: 'loggedInMessage',
            message: 'You\'re logged in.',
            color: 'secondary',
            duration: 5000 // ms.
        },
        loggedOutMessage: {
            id: 'loggedOutMessage',
            message: 'You\'re logged out.',
            color: 'success',
            duration: 5000
        },
        reAuthenticated: {
            id: 'reAuthenticated',
            message: 'You have been reauthenticated.',
            color: 'primary',
            duration: 5000
        },
        reAuthSuccess: {
            id: 'reAuthSuccess',
            message: 'You have been reauthenticated.',
            color: 'primary',
            duration: 5000
        },
        accountExistsDifferentWithCredential: {
            id: 'accountExistsDifferentWithCredential',
            message: 'You have already signed up with a different auth provider for that email.',
            color: 'error',
            duration: 5000,
            disabledBtn:false
        }
    }
})
export const getters = {
    getStatusNotification: state => state.snackbar,
    getTextNotification: state=> state.message,
    getColorNotification: state=> state.color,
    getDurationNotification: state=> state.duration,
    getdisabledBtnNotification: state => state.disabledBtn,
    defaultNotifications: (state) => state.defaultNotifications,
}

export const actions = {
    [CALL_NOTIFICATION]: ({
        commit,
        dispatch
    }, notInfo) => {
        return new Promise((resolve, reject) => { // message,duration,color
            commit(SET_NOTIFICATION, notInfo)
            commit(START_NOTIFICATION)
        })
    },
}

export const mutations = {
    [SET_NOTIFICATION]: (state, payload) => {
        state.message = payload.message
        state.duration = payload.duration|| 6000
        state.color = payload.color || 'primary'
        state.disabledBtn = payload.disabledBtn || true
    },
    [START_NOTIFICATION]: (state) => {
        state.snackbar = true
    },
    [CLOSE_NOTIFICATION]: (state)=>{
        state.snackbar = false
    }
}