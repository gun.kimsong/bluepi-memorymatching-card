# MemoryGame

> bluePi memorygame exam

-------------------------------------------------------------

# For Deployment on your own server

- docker-compose -f docker-compose.yml up --build

# Game at
- localhost:3000

# Api at
- localhost:8000

# Settings 
- in .env-setup you can choose change amount of cards in game (CARD_AMT). 

-------------------------------------------------------------

# For Deploy to cloud run , For 

# Buid container to google cloud docker registry
- cd memorygame; gcloud builds submit --project "bluepi-memorygame" --config=./cloud-build.yml

# Deploy
- gcloud run deploy exam --region asia-northeast1 --project "bluepi-memorygame" --platform=managed --image gcr.io/bluepi-memorygame/exam --allow-unauthenticated;

# Retrafiic to new container 
gcloud alpha run services update-traffic exam --to-latest;

